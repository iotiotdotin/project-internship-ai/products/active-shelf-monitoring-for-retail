# Commands for testing the YOLOv4 model in Google Colab :
### First, download and compile darknet :
!git clone https://github.com/AlexeyAB/darknet

### Change makefile to have GPU and OPENCV enabled :
%cd darknet
!sed -i 's/OPENCV=0/OPENCV=1/' Makefile
!sed -i 's/GPU=0/GPU=1/' Makefile
!sed -i 's/CUDNN=0/CUDNN=1/' Makefile
!sed -i 's/CUDNN_HALF=0/CUDNN_HALF=1/' Makefile

### verify CUDA :
!/usr/local/cuda/bin/nvcc --version

### make darknet :
!make

### Place these files in the darknet directory and run the detector :  
!./darknet detector test obj.data mvtec-test.cfg mvtec_7000.weights D2S_027920.jpg  
(Store the files anywhere but enter the appropiate location in the above command) 

### The predictions gets saved to predictions.jpg in the darknet directory